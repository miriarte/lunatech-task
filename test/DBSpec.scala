import db.{DbLoader, IndexedCountry, LuceneHandler}
import models.{Airports, Countries, Runways}
import org.scalatestplus.play.PlaySpec
import play.api.inject.guice.GuiceApplicationBuilder

class DBSpec extends PlaySpec {
  val injector = new GuiceApplicationBuilder().injector

  val luceneHandler = injector.instanceOf[LuceneHandler]
  val airports = injector.instanceOf[Airports]
  val countries = injector.instanceOf[Countries]
  val runways = injector.instanceOf[Runways]
  val dbLoader = injector.instanceOf[DbLoader]
  dbLoader.awaitLoadDb
  val br = IndexedCountry(302791, "Brazil", "BR")
  val ad = IndexedCountry(302672, "Andorra", "AD")
  val az = IndexedCountry(302621, "Azerbaijan", "AZ")
  val gg = IndexedCountry(302689, "Guernsey", "GG")

  "LuceneHelper" should {
    "find a country by a valid iso code" in {
      luceneHandler.findCountry("GG") must contain(gg)
    }
    "find a country by a valid name" in {
      luceneHandler.findCountry("brazil") must contain(br)
    }
    "return an empty list for a invalid iso code" in {
      luceneHandler.findCountry("XX") must be(List.empty[IndexedCountry])
    }
    "return more than 1 element for a commom query" in {
      luceneHandler.findCountry("a") mustNot have(length(0))
      luceneHandler.findCountry("a") mustNot have(length(1))
    }

    "return 1 country for an incomplete name but unique query" in {
      luceneHandler.findCountry("andor") must be(List(ad))
    }
    "find a country by its id" in {
      luceneHandler.findCountryById(302621) must be(Some(az))
    }
    "return none if id is invalid" in {
      luceneHandler.findCountryById(3) must be(None)
    }
  }

  "Airport Model" should {
    "find all airports for an iso code" in {
      airports.findAllByCountry("BO").size mustNot be(0)
    }
    "find none airports for an invalid iso code" in {
      airports.findAllByCountry("XX").size must be(0)
    }
  }
  "Country model" should {
    "return a list of top 10 countries in number of airports" in {
      countries.getTop10CountriesByAirports.length must be(10)
    }
    "return a list of bottom 10 countries in number of airports" in {
      countries.getBottom10CountriesByAirports.length must be(10)
    }
    "return a list countries with surfaces non empty" in {
      countries.getCountriesWithRunwaySurfaceList.length mustNot be(0)
    }
  }

  "Runway model" should {
    "return a list of top 10 most common ident" in {
      runways.getTop10CommonRunwayIdent.length must be(10)
    }
  }

}
