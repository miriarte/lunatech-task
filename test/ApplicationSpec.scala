import org.scalatestplus.play._
import play.api.db.{DBApi, Databases}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test._
import play.api.test.Helpers._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  * For more information, consult the wiki.
  */
class ApplicationSpec extends PlaySpec with OneAppPerSuite {
    "Routes" should {

      "send 404 on a bad request" in {
        route(app, FakeRequest(GET, "/boum")).map(status(_)) mustBe Some(NOT_FOUND)
      }

    }

    "HomeController" should {

      "render the index page" in {
        val home = route(app, FakeRequest(GET, "/")).get

        status(home) mustBe OK
        contentType(home) mustBe Some("text/html")
        contentAsString(home) must include("What do you plan to do?")
      }

    }

    "AirportController" should {

      "render the index page clean" in {
        val home = route(app, FakeRequest(GET, "/airports")).get

        status(home) mustBe OK
        contentType(home) mustBe Some("text/html")
        contentAsString(home) mustNot include("please select one country or try searching again")
        contentAsString(home) mustNot include("Showing results for")
        contentAsString(home) mustNot include("Country not found. Try again!")
      }

      "render the index with countries to select" in {
        val home = route(app, FakeRequest(POST, "/airports").withFormUrlEncodedBody("query" -> "a")).get

        status(home) mustBe OK
        contentType(home) mustBe Some("text/html")
        contentAsString(home) must include("please select one country or try searching again")
      }

      "render the index with a list of airports" in {
        val home = route(app, FakeRequest(GET, "/airports/DZ/302568")).get

        status(home) mustBe OK
        contentType(home) mustBe Some("text/html")
        contentAsString(home) must include("Showing results for")
      }

      "render the index with an error message for country not found" in {
        val home = route(app, FakeRequest(POST, "/airports").withFormUrlEncodedBody("query" -> "aasdfasdf")).get

        status(home) mustBe OK
        contentType(home) mustBe Some("text/html")
        contentAsString(home) must include("Country not found. Try again!")
      }

      "render the index clean for country id invalid" in {
        val home = route(app, FakeRequest(GET, "/airports/DZ/3")).get

        status(home) mustBe OK
        contentType(home) mustBe Some("text/html")
        contentAsString(home) mustNot include("please select one country or try searching again")
        contentAsString(home) mustNot include("Showing results for")
        contentAsString(home) mustNot include("Country not found. Try again!")
      }

    }

    "ReportController" should {
      "render the index page ok" in {
        val home = route(app, FakeRequest(GET, "/reports")).get

        status(home) mustBe OK
        contentType(home) mustBe Some("text/html")
        contentAsString(home) must include("Top 10 common identifications")
      }
    }

}
