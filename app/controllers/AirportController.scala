package controllers

import javax.inject._

import db.{IndexedCountry, LuceneHandler}
import models.{Airports, ResultSearch}
import play.api.data.Forms._
import play.api.data._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._

case class CountryQuery(query: String)

@Singleton
class AirportController @Inject()(airports: Airports, luceneHandler: LuceneHandler, val messagesApi: MessagesApi) extends Controller with I18nSupport {
  val countryForm = Form(
    mapping(
      "query" -> nonEmptyText
    )(CountryQuery.apply)(CountryQuery.unapply)
  )
  val indexResult = Ok(views.html.airports.index(countryForm, List.empty[IndexedCountry], ResultSearch.empty))

  def index = Action {
    indexResult
  }

  def showResultsFor(country: String, countryId: Long) = Action {
    val luceneResponse = luceneHandler.findCountryById(countryId)
    luceneResponse.map(showResultsForCountry) getOrElse indexResult
  }

  private def showResultsForCountry(country: IndexedCountry) = {
    val result = airports.findAllByCountry(country.code)
    Ok(views.html.airports.index(countryForm, country :: Nil, result))
  }


  def search = Action { implicit request =>
    countryForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.airports.index(formWithErrors, List.empty[IndexedCountry], ResultSearch.empty))
      },
      countryQuery => {
        val listCountries = luceneHandler.findCountry(countryQuery.query)
        listCountries.size match {
          case 1 => showResultsForCountry(listCountries.head)
          case _ => Ok(views.html.airports.index(countryForm.fill(countryQuery), listCountries, ResultSearch.empty))
        }
      }
    )
  }
}
