package controllers

import javax.inject._

import models.{Airports, Countries, Runways}
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ReportController @Inject()(airports: Airports, countries: Countries, runways: Runways)(implicit ec: ExecutionContext) extends Controller {


  def index = Action.async {
    for {
      top10 <- Future(countries.getTop10CountriesByAirports)
      bottom10 <- Future(countries.getBottom10CountriesByAirports)
      runwaysByCountry <- Future(countries.getCountriesWithRunwaySurfaceList)
      commonIdent <- Future(runways.getTop10CommonRunwayIdent)
    } yield
      Ok(views.html.reports.index(top10, bottom10, runwaysByCountry, commonIdent))
  }

}
