package controllers

import javax.inject._

import db.DbLoader
import play.api.mvc._

import scala.concurrent.ExecutionContext



@Singleton
class HomeController @Inject() extends Controller {

  def index = Action {
    Ok(views.html.index())
  }

}
