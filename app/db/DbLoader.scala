package db

import javax.inject.{Inject, Singleton}

import com.google.inject.ImplementedBy
import kantan.csv.ops._
import kantan.csv.generic._
import models._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.Source


@ImplementedBy(classOf[DbLoaderImpl])
trait DbLoader {
  def loadDB: Future[Unit]

  def awaitLoadDb: Unit
}

@Singleton
class DbLoaderImpl @Inject()(airports: Airports, countries: Countries, runways: Runways, indexHandler: LuceneHandler)(implicit ex: ExecutionContext) extends DbLoader {

  def loadDB = {
    for {
      _ <- dropAllDb
      _ <- loadRunways
      _ <- loadAirports
      _ <- loadCountries
    } yield {}
  }

  var alreadyLoaded = indexHandler.listAll().size > 0

  def awaitLoadDb = {
    if (!alreadyLoaded) Await.result(loadDB, 1 minute)
    alreadyLoaded = true
  }

  def dropAllDb: Future[Unit] = {
    for {
      _ <- Future(airports.deleteAll)
      _ <- Future(countries.deleteAll)
      _ <- Future(runways.deleteAll)
      _ <- Future(indexHandler.deleteAll)
    } yield ()
  }


  def loadAirports: Future[Unit] = {
    Future({
      val list = loadCsvAirport()
      airports.addAll(list)
    }
    )
  }


  def loadCountries: Future[Unit] = {
    for {
      list <- Future(loadCsvCountry())
    } yield {
      for {
        _ <- Future(countries.addAll(list))
        _ <- indexCountries(list)
      } yield ()
    }

  }

  def loadRunways: Future[Unit] = {
    Future(
      {
        val list = loadCsvRunway()
        runways.addAll(list)
      }
    )
  }

  def indexCountries(list: List[Country]): Future[Unit] = {
    indexHandler.insertAll(list)
  }

  private def loadCsvRunway() = {
    val rawData: java.net.URL = getClass.getResource("/data/runways.csv")
    val source = Source.fromURL(rawData).mkString
    rawData.unsafeReadCsv[List, Runway](',', true)
  }

  private def loadCsvAirport() = {
    val rawData: java.net.URL = getClass.getResource("/data/airports.csv")
    val source = Source.fromURL(rawData).mkString
    rawData.unsafeReadCsv[List, Airport](',', true)
  }

  private def loadCsvCountry() = {
    val rawData: java.net.URL = getClass.getResource("/data/countries.csv")
    val source = Source.fromURL(rawData).mkString
    rawData.unsafeReadCsv[List, Country](',', true)
  }

  awaitLoadDb

}

