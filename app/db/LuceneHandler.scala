package db

import java.nio.file.Paths
import javax.inject.{Inject, Singleton}

import com.google.inject.ImplementedBy
import com.outr.lucene4s._
import com.outr.lucene4s.field.Field
import com.outr.lucene4s.mapper._
import com.outr.lucene4s.query._
import models.Country
import play.api.inject.ApplicationLifecycle

import scala.concurrent.{ExecutionContext, Future}


@ImplementedBy(classOf[LuceneHandlerImpl])
trait LuceneHandler {
  def findCountryById(countryId: Long): Option[IndexedCountry]

  def deleteAll: Unit

  implicit def countrytoIndexed(country: Country) = IndexedCountry(country.id, country.name, country.code)

  def insertAll(list: List[Country]): Future[Unit]

  val indexedCountries: Searchable[IndexedCountry]

  def findCountry(query: String): List[IndexedCountry]

  def listAll(): List[IndexedCountry]
}

@Singleton
class LuceneHandlerImpl @Inject()(lifecycle: ApplicationLifecycle)(implicit ec: ExecutionContext) extends LuceneHandler {
  val lucene = new Lucene(directory = Some(Paths.get("index")), defaultFullTextSearchable = true, appendIfExists = true)
  val indexedCountries = lucene.create.searchable[SearchableIndexedCountry]
  val DEFAULT_LIMIT = 100

  def insertAll(list: List[Country]): Future[Unit] = {
    Future(
      list.par.foreach(indexedCountries.insert(_).index())
    )
  }

  private def getQueryBuilder = {
    indexedCountries.query().sort(Sort.Score).limit(DEFAULT_LIMIT)
  }

  def findCountry(query: String) = {
    var lowQuery = query.toLowerCase
    val paged = getQueryBuilder.filter(wildcard(lowQuery + "*")).search()
    paged.entries.toList
  }

  def listAll(): List[IndexedCountry] = {
    val paged = getQueryBuilder.search()
    paged.entries.toList
  }

  override def deleteAll: Unit = lucene.deleteAll()


  override def findCountryById(countryId: Long) = {
    val paged = getQueryBuilder.filter(exact(indexedCountries.id(countryId))).search()
    paged.entries.headOption
  }

  lifecycle.addStopHook { () =>
    Future.successful(lucene.dispose())
  }

}

case class IndexedCountry(id: Long, name: String, code: String)

trait SearchableIndexedCountry extends Searchable[IndexedCountry] {
  override def idSearchTerms(ic: IndexedCountry): List[SearchTerm] = exact(id(ic.id)) :: Nil

  def id: Field[Long]

  def code: Field[String]

  def name: Field[String]
}
