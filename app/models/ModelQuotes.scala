package models

import db.DbContext

trait ModelQuotes {
  val db: DbContext

  def closeDB() = db.close()

  import db._

  lazy val airports = quote(querySchema[Airport]("airports", _.id -> "airport_id"))
  lazy val countries = quote(querySchema[Country]("countries", _.id -> "country_id"))
  val runways = quote(querySchema[Runway]("runways", _.id -> "runway_id"))
}
