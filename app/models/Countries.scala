package models

import com.google.inject.{ImplementedBy, Inject}
import db.DbContext


case class Country(id: Long,
                   code: String,
                   name: String,
                   continent: String,
                   wikipediaLink: String,
                   keywords: String)

case class CountryWithCount(country: Country, count: Long)

case class CountryWithSurfaceList(country: Country, surfaceList: List[String])

@ImplementedBy(classOf[CountriesImpl])
trait Countries {
  def deleteAll: Unit

  def find(id: Long): Option[Country]

  def getAll: List[Country]

  def addAll(list: List[Country]): List[Long]

  def getTop10CountriesByAirports: List[CountryWithCount]

  def getBottom10CountriesByAirports: List[CountryWithCount]

  def getCountriesWithRunwaySurfaceList: List[CountryWithSurfaceList]
}

class CountriesImpl @Inject()(val db: DbContext) extends Countries with ModelQuotes {

  import db._


  def find(id: Long): Option[Country] = run(countries.filter(c => c.id == lift(id))).headOption

  def getAll: List[Country] = run(countries)

  def addAll(list: List[Country]): List[Long] = {
    val q = quote {
      liftQuery(list).foreach(a => countries.insert(a))
    }
    run(q)
  }

  def deleteAll: Unit = run(quote {
    countries.delete
  })


  override def getTop10CountriesByAirports: List[CountryWithCount] = {
// this should be this nicer but quill compiler has a bug on it
//
//    val airportQuote = quote {
//      airports.groupBy(_.isoCountry).map {
//        case (isoCode, count) => (isoCode, count.size)
//      }.sortBy(_._2)(Ord.descNullsLast).take(10)
//    }
//
//    run(quote {
//      for {
//        (code, count) <- airportQuote
//        country <- countries if country.code == code
//      } yield (country, count)
//    }).map(i=>CountryWithCount(i._1, i._2))
// so I have to do N+1
    val airportQuote = run(quote {
      airports.groupBy(_.isoCountry).map {
        case (isoCode, count) => (isoCode, count.size)
      }.sortBy(_._2)(Ord.descNullsLast).take(10)
    })

    airportQuote.map{
      case (isoCode,count)=> ({
        val c =  run(quote{
          countries.filter(c=> c.code==lift(isoCode))
        })
        c.head
      },count)
    }.map(i=>CountryWithCount(i._1,i._2))
  }

  override def getBottom10CountriesByAirports: List[CountryWithCount] = {
    val airportQuote = run(quote {
      airports.groupBy(_.isoCountry).map {
        case (isoCode, count) => (isoCode, count.size)
      }.sortBy(_._2)(Ord.ascNullsLast).take(10)
    })

    airportQuote.map{
      case (isoCode,count)=> ({
       val c =  run(quote{
          countries.filter(c=> c.code==lift(isoCode))
        })
        c.head
      },count)
    }.map(i=>CountryWithCount(i._1,i._2))
  }

  override def getCountriesWithRunwaySurfaceList: List[CountryWithSurfaceList] = {
    run(quote {
      for {
        country <- countries
        airport <- airports if country.code == airport.isoCountry
        runway <- runways if airport.id == runway.airportRef
      } yield (country, runway)
    }).groupBy(i => i._1).map(i => CountryWithSurfaceList(i._1, i._2.map(_._2.surface).distinct)).toList
  }
}
