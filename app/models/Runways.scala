package models

import com.google.inject.{ImplementedBy, Inject}
import db.DbContext

case class Runway(id: Long,
                  airportRef: Long,
                  airportIdent: String,
                  lengthFt: Option[Int],
                  widthFt: Option[Int],
                  surface: String,
                  lighted: Option[Int],
                  closed: Option[Int],
                  leIdent: String,
                  leLatitudeDeg: String,
                  leLongitudeDeg: String,
                  leElevationFt: String,
                  leHeadingDegT: String,
                  leDisplacedThresholdFt: String,
                  heIdent: String,
                  heLatitudeDeg: String,
                  heLongitudeDeg: String,
                  heElevationFt: String,
                  heHeadingDegT: String,
                  heDisplacedThresholdFt: String)

case class RunwayIdentWithCount(ident: String, count: Long)

@ImplementedBy(classOf[RunwaysImpl])
trait Runways {
  def deleteAll: Unit

  def find(id: Long): Option[Runway]

  def getAll: List[Runway]

  def addAll(list: List[Runway]): List[Long]

  def getTop10CommonRunwayIdent: List[RunwayIdentWithCount]
}

class RunwaysImpl @Inject()(val db: DbContext) extends Runways with ModelQuotes {

  import db._


  def find(id: Long): Option[Runway] = run(runways.filter(c => c.id == lift(id))).headOption

  def getAll: List[Runway] = run(runways)

  def deleteAll: Unit = run(quote {
    runways.delete
  })

  def addAll(list: List[Runway]): List[Long] = {
    val q = quote {
      liftQuery(list).foreach(a => runways.insert(a))
    }
    run(q)
  }

  override def getTop10CommonRunwayIdent: List[RunwayIdentWithCount] = {
    run(quote {
      runways.groupBy(_.leIdent).map {
        case (ident, count) => (ident, count.size)
      }.sortBy(_._2)(Ord.descNullsLast).take(10)
    }).map { case (ident, count) => RunwayIdentWithCount(ident, count) }
  }

}
