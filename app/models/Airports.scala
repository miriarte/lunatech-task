package models

import com.google.inject.{ImplementedBy, Inject}
import db.DbContext

case class Airport(id: Long,
                   ident: String,
                   `type`: String,
                   name: String,
                   latitudeDeg: Option[Double],
                   longitudeDeg: Option[Double],
                   elevationFt: Option[Int],
                   continent: String,
                   isoCountry: String,
                   isoRegion: String,
                   municipality: String,
                   scheduledService: String,
                   gpsCode: String,
                   iataCode: String,
                   localCode: String,
                   homeLink: String,
                   wikipediaLink: String,
                   keywords: String)

case class AirportWithRunway(airport: Airport, runway: Runway)

case class ResultSearch(listResults: List[AirportWithRunway], size: Long, from: Int, to: Int)

object ResultSearch {
  def empty = ResultSearch(Nil, 0, 0, 0)
}


@ImplementedBy(classOf[AirportsImpl])
trait Airports {
  def deleteAll: Unit

  def findAllByCountry(countryCode: String, from: Int = 0, to: Int = 100): ResultSearch

  def addAll(list: List[Airport]): Unit

  def getAll: List[Airport]

  def find(id: Long): Option[Airport]
}

class AirportsImpl @Inject()(val db: DbContext) extends Airports with ModelQuotes {

  import db._


  def addAll(list: List[Airport]) = {
    val q = quote {
      liftQuery(list).foreach(a => airports.insert(a))
    }
    run(q)
    ()
  }

  def find(id: Long): Option[Airport] = run(airports.filter(c => c.id == lift(id))).headOption

  def getAll: List[Airport] = run(airports)

  def findAllByCountry(countryCode: String, from: Int = 0, to: Int = 100) = {
    val filterQuery = quote {
      airports.join(runways).on((a, r) => a.id == r.airportRef).filter { case (a, r) => a.isoCountry == lift(countryCode) }
    }

    val results = run(quote {
      filterQuery.drop(lift(from)).take(lift(to))
    }).map { case (a, r) => AirportWithRunway(a, r) }
    val size = run(quote {
      filterQuery.size
    })
    ResultSearch(results, size, from, to)
  }


  def deleteAll: Unit = run(quote {
    airports.delete
  })
}
