import com.google.inject.{AbstractModule, Provides}
import db._
import io.getquill.QueryProbing


class Module extends AbstractModule {
  @Provides
  def dbProvider = new DbContext("ctx.prod") with QueryProbing


  override def configure() = {
    bind(classOf[DbLoader]).asEagerSingleton()
  }

}
