name := """lunatech-task"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"
resolvers += Resolver.sonatypeRepo("releases")
resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies += jdbc
libraryDependencies += cache
libraryDependencies += ws
libraryDependencies += evolutions
libraryDependencies ++= Seq(
  "com.h2database" % "h2" % "1.4.190",
  "io.getquill" %% "quill-jdbc" % "1.0.1",
  "com.nrinaudo" %% "kantan.csv" % "0.1.16",
  "com.nrinaudo" %% "kantan.csv-generic" % "0.1.16",
  "com.outr" %% "lucene4s" % "1.4.2"
)
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test


