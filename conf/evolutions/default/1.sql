# Users SCHEMA

# --- !Ups
CREATE TABLE "airports"
(
  "airport_id"        INTEGER PRIMARY KEY,
  "ident"             TEXT,
  "type"              TEXT,
  "name"              TEXT,
  "latitude_deg"      DOUBLE,
  "longitude_deg"     DOUBLE,
  "elevation_ft"      INTEGER,
  "continent"         TEXT,
  "iso_country"       VARCHAR(2),
  "iso_region"        TEXT,
  "municipality"      TEXT,
  "scheduled_service" TEXT,
  "gps_code"          TEXT,
  "iata_code"         TEXT,
  "local_code"        TEXT,
  "home_link"         TEXT,
  "wikipedia_link"    TEXT,
  "keywords"          TEXT
);
CREATE TABLE "countries"
(
  "country_id"     INTEGER,
  "code"           VARCHAR(2),
  "name"           TEXT,
  "continent"      TEXT,
  "wikipedia_link" TEXT,
  "keywords"       TEXT
);
CREATE TABLE "runways"
(
  "runway_id"                 INTEGER,
  "airport_ref"               INTEGER,
  "airport_ident"             TEXT,
  "length_ft"                 INTEGER,
  "width_ft"                  INTEGER,
  "surface"                   TEXT,
  "lighted"                   INTEGER,
  "closed"                    INTEGER,
  "le_ident"                  TEXT,
  "le_latitude_deg"           TEXT,
  "le_longitude_deg"          TEXT,
  "le_elevation_ft"           TEXT,
  "le_heading_deg_t"          TEXT,
  "le_displaced_threshold_ft" TEXT,
  "he_ident"                  TEXT,
  "he_latitude_deg"           TEXT,
  "he_longitude_deg"          TEXT,
  "he_elevation_ft"           TEXT,
  "he_heading_deg_t"          TEXT,
  "he_displaced_threshold_ft" TEXT
);
CREATE INDEX "airports_iso_country_index" ON "airports" ("iso_country");
CREATE INDEX "runways_airport_ref_index" ON "runways" ("airport_ref");
CREATE INDEX "countries_code_index" ON "countries" ("code");


# --- !Downs
DROP TABLE "airports";
DROP TABLE "countries";
DROP TABLE "runways";